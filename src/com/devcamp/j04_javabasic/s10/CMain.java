package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CMain {

	public static void main(String[] args) {
		CAnimal nameA2 = new CFish();
		CPet name2 = new CBird();

		CPerson namePerson = new CPerson();
		namePerson.setId(3);
		namePerson.setAge(30);
		namePerson.setFirstName("Hieu");
		namePerson.setLastName("Nguyen");
		ArrayList<CPet> petsList = new ArrayList<>();
		petsList.add(name2);
		petsList.add((CPet) nameA2);
		namePerson.setPets(petsList);
		System.out.println(namePerson);
		int personId = namePerson.getId();
		System.out.println("Person Id = " + personId);
		int personAge = namePerson.getAge();
		System.out.println("Person Age = " + personAge);

		// ArrayList<Integer> mIntegerArrayList = new ArrayList<>();
		// Integer inte1 = new Integer(10);
		// Integer inte2 = new Integer("20");
		// Integer inte3 = Integer.valueOf("10");
		// Integer inte4 = Integer.valueOf(15);
		// mIntegerArrayList.add(inte1);
		// mIntegerArrayList.add(inte2);
		// mIntegerArrayList.add(inte3);
		// mIntegerArrayList.add(inte4);
		// CIntegerArrayList intergeArrListObject = new CIntegerArrayList(mIntegerArrayList);
		// CMain.testSumable(intergeArrListObject);

		// int[] intArray = { 22, 20, 30 };

		// CIntArray cIntArray = new CIntArray(intArray);
		// CMain.testSumable(cIntArray);
	}
	public static void testSumable (ISumable paramISum) {
		  System.out.println(paramISum.getSum());
	} 
}

